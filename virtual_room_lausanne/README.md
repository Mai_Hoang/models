# Virtual Room:

This folder contains a model of a room that is designed as an extension of the DisplayWall room beyond its screen.
, the screen being a window through which people can observe neurorobotics experiments.

The graphical model is blender/virtual_room.blend  designed using Blender and exported as collada file (.dae).
The physical model is model.sdf and refers to the .dae file in its visual section.
The file virtual_room.sdf integrates the physical model in a Gazebo 3D scene and creates lights for the model.

To import the model in Gazebo, copy the virtual_room directory into ~/.gazebo/Models.
After starting Gazebo the model will be avaiable in the models list.

To watch the room, you can also simply run 
$ gazebo path_to_your_virtual_room_dir/virtual_room.sdf
in a terminal.




