<?xml version="1.0"?>
<model xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.humanbrainproject.eu/SP10/2017/robot_model_config" xsi:schemaLocation="http://schemas.humanbrainproject.eu/SP10/2017/robot_model_config ../robot_model_configuration.xsd">
    <name>Arm robot</name>
    <version>1.0</version>
    <thumbnail>thumbnail.png</thumbnail>
    <sdf version="1.5">arm_robot.sdf</sdf>

    <author>
        <name>J. Camilo Vasquez Tieck</name>
        <email>tieck@fzi.de</email>
        <name>Igor Peric</name>
        <email>peric@fzi.de</email>
    </author>

    <description>
        Model of an idustrial arm and hand robot.
        The arm: Schunk Powerball Lightweight Arm LWA 4P
        The hand: Schunk SVH 5-finger hand.
    </description>

    <website>https://www.fzi.de/en/research/projekt-details/hollie/</website>

    <documentation>
        <sensors>
            <!-- <sensor name="" type="" /> -->
        </sensors>

        <actuators>
          <actuator name="hollie_real_base_x_joint" type="motor" />
          <actuator name="hollie_real_left_arm_0_joint" type="motor" />
          <actuator name="hollie_real_left_arm_1_joint" type="motor" />
          <actuator name="hollie_real_left_arm_2_joint" type="motor" />
          <actuator name="hollie_real_left_arm_3_joint" type="motor" />
          <actuator name="hollie_real_left_arm_4_joint" type="motor" />
          <actuator name="hollie_real_left_arm_5_joint" type="motor" />
          <actuator name="hollie_real_left_arm_6_joint" type="motor" />
          <actuator name="hollie_real_left_hand_base_joint" type="motor" />
          <actuator name="hollie_real_left_hand_f4" type="motor" />
          <actuator name="hollie_real_left_hand_Thumb_Opposition" type="motor" />
          <actuator name="hollie_real_left_hand_Thumb_Helper" type="motor" />
          <actuator name="hollie_real_left_hand_Thumb_Flexion" type="motor" />
          <actuator name="hollie_real_left_hand_j3" type="motor" />
          <actuator name="hollie_real_left_hand_j4" type="motor" />
          <actuator name="hollie_real_left_hand_f1" type="motor" />
          <actuator name="hollie_real_left_hand_f2" type="motor" />
          <actuator name="hollie_real_left_hand_f3" type="motor" />
          <actuator name="hollie_real_left_hand_index_spread" type="motor" />
          <actuator name="hollie_real_left_hand_Index_Finger_Proximal" type="motor" />
          <actuator name="hollie_real_left_hand_Index_Finger_Distal" type="motor" />
          <actuator name="hollie_real_left_hand_j14" type="motor" />
          <actuator name="hollie_real_left_hand_middle_spread_dummy" type="motor" />
          <actuator name="hollie_real_left_hand_Middle_Finger_Proximal" type="motor" />
          <actuator name="hollie_real_left_hand_Middle_Finger_Distal" type="motor" />
          <actuator name="hollie_real_left_hand_j15" type="motor" />
          <actuator name="hollie_real_left_hand_j5" type="motor" />
          <actuator name="hollie_real_left_hand_f5" type="motor" />
          <actuator name="hollie_real_left_hand_f6" type="motor" />
          <actuator name="hollie_real_left_hand_f7" type="motor" />
          <actuator name="hollie_real_left_hand_Finger_Spread" type="motor" />
          <actuator name="hollie_real_left_hand_Pinky" type="motor" />
          <actuator name="hollie_real_left_hand_j13" type="motor" />
          <actuator name="hollie_real_left_hand_j17" type="motor" />
          <actuator name="hollie_real_left_hand_f8" type="motor" />
          <actuator name="hollie_real_left_hand_ring_spread" type="motor" />
          <actuator name="hollie_real_left_hand_Ring_Finger" type="motor" />
          <actuator name="hollie_real_left_hand_j12" type="motor" />
          <actuator name="hollie_real_left_hand_j16" type="motor" />
        </actuators>

        <publication
            title="Towards Grasping with Spiking Neural Networks for Anthropomorphic Robot Hands"
            authors="Vasquez Tieck, J. Camilo and Donat, Heiko and Kaiser, Jacques and Peric, Igor and Ulbrich, Stefan and Roennau, Arne and Marius, Zöllner and Dillmann, Rüdiger"
            url="https://link.springer.com/chapter/10.1007/978-3-319-68600-4_6" />
        <publication
            title="Connecting Artificial Brains to Robots in a Comprehensive Simulation Framework: The Neurorobotics Platform"
            authors="Falotico, Egidio and Vannucci, Lorenzo and Ambrosano, Alessandro and Albanese, Ugo and Ulbrich, Stefan and {Vasquez Tieck}, Juan Camilo and Hinkel, Georg and Kaiser, Jacques and Peric, Igor and Denninger, Oliver and Cauli, Nino and Kirtay, Murat and Roennau, Arne and Klinker, Gudrun and {Von Arnim}, Axel and Guyot, Luc and Peppicelli, Daniel and Martinez-Cañada, Pablo and Ros, Eduardo and Maier, Patrick and Weber, Sandro and Huber, Manuel and Plecher, David and Röhrbein, Florian and Deser, Stefan and Roitberg, Alina and van der Smagt, Patrick and Dillman, Rüdiger and Levi, Paul and Laschi, Cecilia and Knoll, Alois C. and Gewaltig, Marc-Oliver"
            url="https://www.frontiersin.org/articles/10.3389/fnbot.2017.00002/full" />
    </documentation>
</model>

